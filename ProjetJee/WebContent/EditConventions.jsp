<%@page import="models.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${sessionScope.CurrentUser == null}">
<c:redirect url = "403.html"/>
</c:if>
 <c:import url="Header.jsp"></c:import>  
	<div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Conventions</li>
        </ol>
        <center>
        <p class="text-success">
        <c:out value="${requestScope.success}"/>
        </p>
        </center>
        <center>
        <p class="text-danger">
        <c:out value="${requestScope.erreur}"/>
        </p>
        </center>
<div class="card mb-3 p-3">
		<jsp:useBean id="UpdateConv" class="models.Convention" scope="session"></jsp:useBean>
<form action='UpdateConv' name='EditConv'  method='POST'> 

           <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
	            <div class="form-group">
	              <label for="types">Type de convention</label>
	              <select id="types" name="type" class="form-control">
	              <jsp:useBean id="TypeConvention" class="models.TypeConvention"/>
	               <c:forEach items="${TypeConvention.getTypes()}" var="value">
					  <option value="<c:out value="${value}"/>"><c:out value="${value}"/></option>
				   </c:forEach>
	              </select>

	            </div>
              </div>
              <div class="col-md-6">
	            <div class="form-group">
	              <label for="DateEdition">Date Edition</label>
	              <input type="date" id="DateEdition" name="DateEdition" class="form-control" value="<jsp:getProperty name="UpdateConv" property="dateEditionConvention"/>" required="required">
	            </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-group">
              <label for="objet">Objet du convention</label>
              <input type="text" id="objet" value='<jsp:getProperty name="UpdateConv" property="objet"/>' name="objet" class="form-control" required="required">
            </div>
          </div>
          <input name="idConv" type="number" id="idConv" value='<jsp:getProperty name="UpdateConv" property="id"/>' hidden="hidden">
                      <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-group">
	              <label for="DateEntreEnVigeur">Date d'entree en vigeur</label>
	              <input type="date" id="DateEntreEnVigeur" value='<jsp:getProperty name="UpdateConv" property="dateVigueurConvention"/>'  name="DateEntreEnVigeur" class="form-control"  required="required">
	            </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
	              <label for="DateExpiration">Date d'expiration</label>
	              <input type="date" id="DateExpiration" value='<jsp:getProperty name="UpdateConv" property="dateVigueurConvention"/>' name="DateExpiration" class="form-control"  required="required">
	            </div>
              </div>
              </div>
              </div>
<div class="row">
<div class="col-md-7">
          <nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Choisir les participants</a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Nouveaux participants</a>
  </div>
</nav>
<div class="tab-content tab-info" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
  <br>
              <div class="form-group">
              <label for="ParticipantsListe">Liste des participant</label>
              <select id="ParticipantsListe" name="ParticipantsListe" class="form-control select2" required>
              <jsp:useBean id="Participants" class="API.APIParticipant"/>
               <c:forEach items="${Participants.GetAllParticipant()}" var="p">
				  <option value="<c:out value="${p.id}"/>"><c:out value="${p.nom}"/></option>
			   </c:forEach>
              </select>
            </div>
                            <div class="form-group">
                <label for="DateSign">Date signature</label>
                  <input type="date" id="DateSignOld" class="form-control" >         
                </div>
               <div class="form-group">
              <button type="button" class="btn btn-info" id="AddOldParti">Ajouter</button>
              </div>
</div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  <br>
  				          <div class="form-group">
				            <div class="form-group">
				            <label for="NewParticipant">Nom participant</label>
				              <input type="text" id="NewParticipant" name="NewParticipant" class="form-control" >
								
				            </div>
				          </div>
				          		                <div class="form-group">
		                <label for="DateSign">Date signature</label>
		                  <input type="date" id="DateSignNew" class="form-control" >         
		                </div>
		                		              <div class="form-group">

		             <button type="button" id="addNewParticipant" class="btn btn-info" >Ajouter</button>
		              </div>
</div>
</div>

</div>
<div class="col-md-4">
<div id="ListPartDiv">
<label for="DateSign">Participants</label>
<ul class="list-group list-group-flush" id="ChosenParts">
	                    	<c:forEach items="${UpdateConv.listConventionParticipant}" var="partConv" varStatus="rowCount">
				             <li class="list-group-item" id='Liste_selected_<c:out value="${partConv.participant.id}"/>'><c:out value="${partConv.participant.nom}"/> | <c:out value="${partConv.dateSignature}"/> <button class='close' onclick='deletePart(<c:out value="${partConv.participant.id}"/>)' type='button' aria-label='Close'><span aria-hidden='true' style='color:red;'>x</span></button>  </li>
				            </c:forEach>
</ul>
</div>
</div>
</div>

<div class="form-group">
          <button type="submit"  class="btn btn-success">Valider la convention</button>
</div>

     <select id="SelectedParticipant" name="SelectedParticipant" multiple hidden="hidden">
     	                    	<c:forEach items="${UpdateConv.listConventionParticipant}" var="partConv" varStatus="rowCount">
     	                    	<option id='selected_<c:out value="${partConv.participant.id}"/>' value='<c:out value="${partConv.participant.id}"/>_<c:out value="${partConv.dateSignature}"/>_<c:out value="${partConv.id}"/>' name='SelectedParticipant' selected ></option>
				            </c:forEach>
	</select>

                </form>
	</div>
</div>
</div>
 <c:import url="footer.jsp"></c:import>  
 	              <script type="text/javascript">
 	              
 	              
 	             var added=<c:out value="${UpdateConv.listConventionParticipant.size()}"/> ;
 	            added=added+1
 	            $("#AddOldParti").click(function() {
 	           	 if(added<4){
 	           		 var id = $("#ParticipantsListe").val();
 	           		 var date = $("#DateSignOld").val(); 
 	           		 if(date!=""){
 	           			 $("#ListPartDiv").removeAttr("hidden");
 	           			 $("#ChosenParts").append("<li id='Liste_selected_"+added+"' class='list-group-item'>"+$("#ParticipantsListe option:selected").text()+" | "+date+" <button class='close' onclick='deletePart("+added+")' type='button' aria-label='Close'><span aria-hidden='true' style='color:red;'>x</span></button></li>")
 	           			 $("#SelectedParticipant").append("<option id='selected_"+added+"' value='"+id+"_"+date+"_0' name='SelectedParticipant' selected ></option>")
 	           			 $("#ParticipantsListe option:selected").remove();
 	           			 added++;
 	           		 }else{
 	           			 alert("la date du signature est obligatoire !! ")
 	           		 }
 	           	 }else{
 	           		 alert("Nombre maximum de participant est attient ")
 	           	 }

 	            });
 	            
 	            function deletePart(id){
 	           	 $("#selected_"+id).remove();
 	           	 $("#Liste_selected_"+id).remove();
 	           	 added--;
 	            }

 	            $("#addNewParticipant").click(function() {
 	           	 if(added<4){
 	           		 var name = $("#NewParticipant").val();
 	           		 var date = $("#DateSignNew").val(); 
 	           		 if(date!="" && name!=""){
 	           			 $("#ListPartDiv").removeAttr("hidden");
 	           			 $("#ChosenParts").append("<li class='list-group-item' id='Liste_selected_"+added+"'>"+name+" | "+date+"<button class='close' onclick='deletePart("+added+")' type='button' aria-label='Close'><span aria-hidden='true' style='color:red;'>x</span></button></li>")
 	           			 $("#SelectedParticipant").append("<option id='selected_"+added+"' value='"+name+"_"+date+"' name='SelectedParticipant' selected ></option>")
 	           			 added++;
 	           		 }else{
 	           			 alert("la date du signature et le nom sont des champs obligatoires !! ")
 	           		 }
 	           	 }else{
 	           		 alert("Nombre maximum de participant est attient ")
 	           	 }
 	            });
 	            
 	             $(document).ready(function() {
 		              $('#types').val('<jsp:getProperty name="UpdateConv" property="type"/>')
 	           });

	              </script>