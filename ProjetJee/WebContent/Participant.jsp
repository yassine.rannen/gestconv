<%@page import="models.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <c:if test="${sessionScope.CurrentUser == null}">
<c:redirect url = "403.html"/>
</c:if>
 <c:import url="Header.jsp"></c:import>  
	<div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="Convention.jsp">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Conventions</li>
        </ol>
        <center>
        <p class="text-success">
        <c:out value="${requestScope.success}"/>
        </p>
        </center>
        <center>
        <p class="text-danger">
        <c:out value="${requestScope.erreur}"/>
        </p>
        </center> 
                <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Liste des Participant			
            </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nom</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Nom</th>
                  </tr>
                </tfoot>
                <tbody>

				<jsp:useBean id="Participants" class="API.APIParticipant"/>
	               <c:forEach items="${Participants.GetAllParticipant()}" var="p">
	                  <tr>
	                    <td><c:out value="${p.id}"/></td>
	                    <td><c:out value="${p.nom}"/></td>
	                  </tr>
				   </c:forEach>
                </tbody>
              </table>
            </div>
          </div>
        </div>

</div>
</div>




 <c:import url="footer.jsp"></c:import>  

