<%@page import="models.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <c:if test="${sessionScope.CurrentUser == null}">
<c:redirect url = "403.html"/>
</c:if>
 <c:import url="Header.jsp"></c:import>  
	<div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="Convention.jsp">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Conventions</li>
        </ol>
        <center>
        <p class="text-success">
        <c:out value="${requestScope.success}"/>
        </p>
        </center>
        <center>
        <p class="text-danger">
        <c:out value="${requestScope.erreur}"/>
        </p>
        </center>
        <!-- Icon Cards-->
           <center>
           <button type="button" data-toggle="modal" data-target="#AddConventionModal" class="btn btn-info"> Ajouter une nouvelle convention</button>
           </center> 
           <br>  
                <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Liste des conventions			
            </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Objet</th>
                    <th>Entre en vigeur</th>
                    <th>Expiration</th>
                    <th>date Edition</th>
                    <th>Participants</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Objet</th>
                    <th>Entre en vigeur</th>
                    <th>Expiration</th>
                    <th>date Edition</th>
                    <th>Participants</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>

				<jsp:useBean id="Conventions" class="API.APIConvention"/>
	               <c:forEach items="${Conventions.GetAllConventions()}" var="c">
	                  <tr>
	                    <td><c:out value="${c.id}"/></td>
	                    <td><c:out value="${c.type}"/></td>
	                    <td><c:out value="${c.objet}"/></td>
	                    <td><c:out value="${c.dateVigueurConvention}"/></td>
	                    <td><c:out value="${c.dateExpirationConvention}"/></td>
	                    <td><c:out value="${c.dateEditionConvention}"/></td>
	                    <td>      
	                    	<a onclick="GetConv(<c:out value="${c.id}"/>)" ><i style="color:blue;cursor:pointer;" class="fas fa-users"></i></a>   
			            </td>
	                    <td><a href='EditConvention?id=<c:out value="${c.id}"/>' style='color:green;cursor:pointer;' ><i  class='fas fa-edit'></i></a>
							<a onclick='setDeleteInput(<c:out value="${c.id}"/>)' style='color:red;cursor:pointer;' > <i class='fas fa-trash'></i></a>
	                    </td>
	                  </tr>
				   </c:forEach>
                </tbody>
              </table>
            </div>
          </div>
        </div>

 <div class="modal fade ParticipantModal" id="ParticipantModalid" tabindex="-1" role="dialog" aria-labelledby="ParticipantModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ParticipantModalLabel">Liste des participants </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
       <table>
       <thead class="table" id="ListPartParConv">
      
       </thead>
       </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>

    </div>
  </div>
</div>       
<div class="modal fade DeleteConventionModal" id="#DeleteConventionModalid" tabindex="-1" role="dialog" aria-labelledby="DeleteConventionModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="DeleteConventionModalLabel">Confirmation </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action='DeleteConvention' method='POST'>
      <input type="number" id="idConventionDelete" name="idConventionDelete" hidden="hidden">
            <div class="modal-body">
       <p>confirmer vous la suppression de cette convention ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-danger">Supprimer</button>
      </div>
      </form>

    </div>
  </div>
</div>
 <div class="modal fade" id="AddConventionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ajouter une convention</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">X</span>
          </button>
        </div>
                       <form action='AddConvention' name='FormConv'  method='POST'>   
        <div class="modal-body">

  
         
           <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
	            <div class="form-group">
	              <label for="types">Type de convention</label>
	              <select id="types" name="type" class="form-control">
	              <jsp:useBean id="TypeConvention" class="models.TypeConvention"/>
	               <c:forEach items="${TypeConvention.getTypes()}" var="value">
					  <option><c:out value="${value}"/></option>
				   </c:forEach>
	              </select>
	            </div>
              </div>
              <div class="col-md-6">
	            <div class="form-group">
	              <label for="DateEdition">Date Edition</label>
	              <input type="date" id="DateEdition" name="DateEdition" class="form-control"  required="required">
	            </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-group">
              <label for="objet">Objet du convention</label>
              <input type="text" id="objet" name="objet" class="form-control" required="required">
            </div>
          </div>
                      <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-group">
	              <label for="DateEntreEnVigeur">Date d'entree en vigeur</label>
	              <input type="date" id="DateEntreEnVigeur" name="DateEntreEnVigeur" class="form-control"  required="required">
	            </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
	              <label for="DateExpiration">Date d'expiration</label>
	              <input type="date" id="DateExpiration" name="DateExpiration" class="form-control"  required="required">
	            </div>
              </div>
              </div>
              </div>
<div class="row">
<div class="col-md-7">
          <nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Choisir les participants</a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Nouveaux participants</a>
  </div>
</nav>
<div class="tab-content tab-info" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
  <br>
              <div class="form-group">
              <label for="ParticipantsListe">Liste des participant</label>
              <select id="ParticipantsListe" class="form-control select2" >
              <jsp:useBean id="Participants" class="API.APIParticipant"/>
               <c:forEach items="${Participants.GetAllParticipant()}" var="p">
				  <option value="<c:out value="${p.id}"/>"><c:out value="${p.nom}"/></option>
			   </c:forEach>
              </select>
            </div>
                            <div class="form-group">
                <label for="DateSign">Date signature</label>
                  <input type="date" id="DateSignOld" class="form-control" >         
                </div>
               <div class="form-group">
              <button type="button" class="btn btn-info" id="AddOldParti">Ajouter</button>
              </div>
</div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  <br>
  				          <div class="form-group">
				            <div class="form-group">
				            <label for="NewParticipant">Nom participant</label>
				              <input type="text" id="NewParticipant" name="NewParticipant" class="form-control" >
								
				            </div>
				          </div>
				          		                <div class="form-group">
		                <label for="DateSign">Date signature</label>
		                  <input type="date" id="DateSignNew" class="form-control" >         
		                </div>
		                		              <div class="form-group">

		             <button type="button" id="addNewParticipant" class="btn btn-info" >Ajouter</button>
		              </div>
</div>
</div>

</div>
<div class="col-md-5">
<div id="ListPartDiv" hidden="hidden">
<label for="DateSign">Participants</label>
<ul class="list-group list-group-flush" id="ChosenParts">
</ul>
</div>
</div>
</div>


     <select id="SelectedParticipant" hidden="hidden" name="SelectedParticipant" multiple>
	</select>

		</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
          <button type="submit"  class="btn btn-success">Valider la convention</button>
        </div>
                </form>
      </div>
    </div>
  </div>           
</div>
</div>




 <c:import url="footer.jsp"></c:import>  
 
 <script>
 //AddOldParti
 var added=0;
 $("#AddOldParti").click(function() {
	 if(added<4){
		 var id = $("#ParticipantsListe").val();
		 var date = $("#DateSignOld").val(); 
		 if(date!=""){
			 $("#ListPartDiv").removeAttr("hidden");
			 $("#ChosenParts").append("<li id='Liste_selected_"+added+"' class='list-group-item'>"+$("#ParticipantsListe option:selected").text()+" | "+date+" <button class='close' onclick='deletePart("+added+")' type='button' aria-label='Close'><span aria-hidden='true' style='color:red;'>x</span></button></li>")
			 $("#SelectedParticipant").append("<option id='selected_"+added+"' value='"+id+"_"+date+"' name='SelectedParticipant' selected ></option>")
			 added++;
		 }else{
			 alert("la date du signature est obligatoire !! ")
		 }
	 }else{
		 alert("Nombre maximum de participant est attient ")
	 }

 });
 
 function GetConv(id){
	 $("#ListPartParConv").empty();
	 $.ajax({
		  type: 'GET',
		  url: "/ProjetJee/GetParticipantByConv",
		  data: {
			  id: id,
		  },
		  success: function( data ) {
			  $("#ListPartParConv").append("<tr scope='row'> <th scope='col'>Participant</th><th scope='col'>Date signature</th></tr>");
			  $.each(data.split('&'), function(){
			      
			       
				  $("#ListPartParConv").append(" <tr scope='row'><td>"+this.split("_")[0]+"</td><td>"+this.split("_")[1]+"</td></tr>");
				 
				})
				 $(".ParticipantModal").modal("toggle");
		  },
		  error:function(data,status,er) {
		    alert("error: "+data+" status: "+status+" er:"+er);
		   }
		 });
 }
 function deletePart(id){
	 $("#selected_"+id).remove();
	 $("#Liste_selected_"+id).remove();
	 added--;
 }
function setDeleteInput(id){
	$("#idConventionDelete").val(id);
	$(".DeleteConventionModal").modal("toggle");
}
 $("#addNewParticipant").click(function() {
	 if(added<4){
		 var name = $("#NewParticipant").val();
		 var date = $("#DateSignNew").val(); 
		 if(date!="" && name!=""){
			 $("#ListPartDiv").removeAttr("hidden");
			 $("#ChosenParts").append("<li class='list-group-item' id='Liste_selected_"+added+"'>"+name+" | "+date+"<button class='close' onclick='deletePart("+added+")' type='button' aria-label='Close'><span aria-hidden='true' style='color:red;'>x</span></button></li>")
			 $("#SelectedParticipant").append("<option id='selected_"+added+"' value='"+name+"_"+date+"' name='SelectedParticipant' selected ></option>")
			 added++;
		 }else{
			 alert("la date du signature et le nom sont des champs obligatoires !! ")
		 }
	 }else{
		 alert("Nombre maximum de participant est attient ")
	 }
 });
 $(document).ready(function() {
     $('#Participants').select2({
         width: '100%'
     });
});
 </script>
