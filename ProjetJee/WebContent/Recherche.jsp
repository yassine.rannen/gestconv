<%@page import="models.User"%>
<%@page import="models.RechercheModel"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${sessionScope.CurrentUser == null}">
<c:redirect url = "403.html"/>
</c:if>
 <c:import url="Header.jsp"></c:import>  
	<div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Recherche</li>
        </ol>
                <center>
        <p class="text-success">
        <c:out value="${requestScope.success}"/>
        </p>
        </center>
                <center>
        <p class="text-danger">
        <c:out value="${requestScope.erreur}"/>
        </p>
        </center>
        
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-search"></i>
            Recherche Avanc�e			
            </div>
             <div class="card-body">
             <form action='Recherche' name='RechercheConv'  method='POST'>
                 <div class="form-group">
		            <div class="form-row">
		            		              <div class="col-md-3">
		                  <div class="form-group">
				            <div class="form-group">
				              <label for="objet">Objet du convention</label>
				              <input type="text" id="objet" name="objet" class="form-control">
				            </div>
				          </div>
		              </div>
		              <div class="col-md-3">
			            <div class="form-group">
			              <label for="types">Type de convention</label>
			              <select id="types" name="type" class="form-control">
			              <option value="%">Choisir le type</option>
			              <jsp:useBean id="TypeConvention" class="models.TypeConvention"/>
			               <c:forEach items="${TypeConvention.getTypes()}" var="value">
							  <option value="<c:out value="${value}"/>"><c:out value="${value}"/></option>
						   </c:forEach>
			              </select>	
			            </div>
		              </div>

		              <div class="col-md-3">
			              <div class="form-group">
			              <label for="ParticipantsListe">Liste des participant</label>
			              <select id="ParticipantsListe" name="ParticipantsListe" class="form-control select2">
			              <option value="%">Choisir un participant</option>
			              <jsp:useBean id="Participants" class="API.APIParticipant"/>
			               <c:forEach items="${Participants.GetAllParticipant()}" var="p">
							  <option value="<c:out value="${p.id}"/>"><c:out value="${p.nom}"/></option>
						   </c:forEach>
			              </select>
			            </div>
		              </div>
		              		              		              <div class="col-md-3">
		                              <div class="form-group">
	              <label for="DateSignature">Date de signiature</label>
	              <input type="date" id="DateSignature"  name="DateSignature" class="form-control">
	            </div>
		              </div>
		            </div>
		            <div class="form-row">
		            <div class="col-md-3">			          
		              <div class="form-group">
			              <label for="DateEdition">Date Edition</label>
			              <input type="date" id="DateEdition" name="DateEdition" class="form-control">
			            </div></div>
		             <div class="col-md-3">
		                             <div class="form-group">
	              <label for="DateEntreEnVigeur">Date d'entree en vigeur</label>
	              <input type="date" id="DateEntreEnVigeur"  name="DateEntreEnVigeur" class="form-control">
	            </div>
	            </div>
		              <div class="col-md-3">
		                              <div class="form-group">
	              <label for="DateExpiration">Date d'expiration</label>
	              <input type="date" id="DateExpiration"  name="DateExpiration" class="form-control">
	            </div>
		              </div>
		              		              <div class="col-md-3">
		                              <div class="form-group">
	              <label for="btnSubmit">.</label>
	              <button id="btnSubmit" type="submit" class="form-control btn btn-info">Lan�er la recherche <i class="fas fa-search"></i></button>
	            </div>
		              </div>
		            </div>
		          </div>
              </form>
             </div>

	</div>
	<div class="card mb-3" id="Recherche_Section">
	<c:if test="${sessionScope.ListRes != null}">
          <div class="card-header">
            <i class="fas fa-search"></i>
            R�sultat de recherche : <b><c:out value="${sessionScope.ListRes.size()}"/>  </b> conventions	
            </div>
             <div class="card-body">         
				
				<br>
				<table class="table">
				                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Objet</th>
                    <th>Entre en vigeur</th>
                    <th>Expiration</th>
                    <th>date Edition</th>
                    <th>Participants</th>
                    <th>Action</th>
                  </tr>
                </thead>
					<c:forEach items="${sessionScope.ListRes}" var="res">
					
					<tr>
	                    <td><c:out value="${res.id}"/></td>
	                    <td><c:out value="${res.type}"/></td>
	                    <td><c:out value="${res.objet}"/></td>
	                    <td><c:out value="${res.dateVigueurConvention}"/></td>
	                    <td><c:out value="${res.dateExpirationConvention}"/></td>
	                    <td><c:out value="${res.dateEditionConvention}"/></td>
	                    					      <td>      
	                    	<a onclick="GetConv(<c:out value="${res.id}"/>)" ><i style="color:blue;cursor:pointer;" class="fas fa-users"></i></a>   
			            </td>
	                    <td><a href='EditConvention?id=<c:out value="${res.id}"/>' style='color:green;cursor:pointer;' ><i  class='fas fa-edit'></i></a>
							<a onclick='setDeleteInput(<c:out value="${res.id}"/>)' style='color:red;cursor:pointer;' > <i class='fas fa-trash'></i></a>
	                    </td>
	                  </tr>


				   </c:forEach>
				   	</table>
				
             </div>
             </c:if>
    </div>

</div>
</div>
 <div class="modal fade ParticipantModal" id="ParticipantModalid" tabindex="-1" role="dialog" aria-labelledby="ParticipantModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ParticipantModalLabel">Liste des participants </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
       <table>
       <thead class="table" id="ListPartParConv">
      
       </thead>
       </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>

    </div>
  </div>
</div>       
<div class="modal fade DeleteConventionModal" id="#DeleteConventionModalid" tabindex="-1" role="dialog" aria-labelledby="DeleteConventionModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="DeleteConventionModalLabel">Confirmation </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action='DeleteConvention' method='POST'>
      <input type="number" id="idConventionDelete" name="idConventionDelete" hidden="hidden">
            <div class="modal-body">
       <p>confirmer vous la suppression de cette convention ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-danger">Supprimer</button>
      </div>
      </form>

    </div>
  </div>
</div>
 <c:import url="footer.jsp"></c:import>  
 
  <script>
 //AddOldParti
 
 function GetConv(id){
	 $("#ListPartParConv").empty();
	 $.ajax({
		  type: 'GET',
		  url: "/ProjetJee/GetParticipantByConv",
		  data: {
			  id: id,
		  },
		  success: function( data ) {
			  $("#ListPartParConv").append("<tr scope='row'> <th scope='col'>Participant</th><th scope='col'>Date signature</th></tr>");
			  $.each(data.split('&'), function(){
			      
			       
				  $("#ListPartParConv").append(" <tr scope='row'><td>"+this.split("_")[0]+"</td><td>"+this.split("_")[1]+"</td></tr>");
				 
				})
				 $(".ParticipantModal").modal("toggle");
		  },
		  error:function(data,status,er) {
		    alert("error: "+data+" status: "+status+" er:"+er);
		   }
		 });
 }
function setDeleteInput(id){
	$("#idConventionDelete").val(id);
	$(".DeleteConventionModal").modal("toggle");
}
 </script>