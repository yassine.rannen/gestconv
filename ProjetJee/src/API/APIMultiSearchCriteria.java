package API;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.transform.DistinctRootEntityResultTransformer;
import org.hibernate.transform.ResultTransformer;

import models.Convention;
import utils.HibernateUtils;

public class APIMultiSearchCriteria {

	@SuppressWarnings("deprecation")
	public static HashSet<Convention> getConventionsByCustomSearch(String type, String dateEditionConvention, String objet,
			String dateVigueurConvention, String dateExpirationConvention, String dateSignature, String idpart) {
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			Query query = session.createQuery(
					"from Convention c, ConventionParticipant cp, Participant p where c.id=cp.convention.id and p.id=cp.participant.id and c.type like :type and c.dateEditionConvention like :dateEditionConvention and c.objet like :objet and c.dateVigueurConvention like :dateVigueurConvention and c.dateExpirationConvention like :dateExpirationConvention and cp.dateSignature like :dateSignature and p.id like :idPart");
			query.setString("type", type);
			query.setString("dateEditionConvention", dateEditionConvention);
			query.setString("objet", objet);
			query.setString("dateVigueurConvention", dateVigueurConvention);
			query.setString("dateExpirationConvention", dateExpirationConvention);
			query.setString("dateSignature", dateSignature);
			query.setString("idPart",  idpart);
			//query.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			HashSet<Convention> listConvention = new HashSet();
			if (query.list() != null) {
				List<Object[]> listObject = query.list();
				for (Object[] o : listObject) {
					Convention Conv = (Convention) o[0];
					listConvention.add(Conv);
				}
			}
			return listConvention;
		} catch (Exception e) {
			System.out.println("error : could not establish a session");
			e.printStackTrace();
			return null;
		}
	}
	@SuppressWarnings("deprecation")
	public static HashSet<Convention> getConventionsByKeyWord(String KeyWord) {
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			Query query = session.createQuery(
					"from Convention c, ConventionParticipant cp, Participant p where c.id=cp.convention.id and p.id=cp.participant.id and (c.type like :type or c.dateEditionConvention like :dateEditionConvention or c.objet like :objet or c.dateVigueurConvention like :dateVigueurConvention or c.dateExpirationConvention like :dateExpirationConvention or cp.dateSignature like :dateSignature or p.nom like :nom)");
			KeyWord="%"+KeyWord+"%";
			query.setString("type", KeyWord);
			query.setString("dateEditionConvention", KeyWord);
			query.setString("objet", KeyWord);
			query.setString("dateVigueurConvention", KeyWord);
			query.setString("dateExpirationConvention", KeyWord);
			query.setString("dateSignature", KeyWord);
			query.setString("nom", KeyWord);
			HashSet<Convention> listConvention = new HashSet();//i used the hash set to keep the result distinct because i didn't get it to work with select distinct
			if (query.list() != null) {
				List<Object[]> listObject = query.list();
				for (Object[] o : listObject) {
					Convention Conv = (Convention) o[0];
					listConvention.add(Conv);
				}
			}
			return listConvention;
		} catch (Exception e) {
			System.out.println("error : no result fount");
			e.printStackTrace();
			return null;
		}
	}
}
