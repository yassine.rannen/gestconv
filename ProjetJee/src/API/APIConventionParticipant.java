package API;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import models.ConventionParticipant;
import utils.HibernateUtils;

public class APIConventionParticipant {

	public static void SaveConventionParticipant(ConventionParticipant ConvPart) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(ConvPart);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		}
	}

	public static ConventionParticipant loadConventionParticipant(Integer id) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			ConventionParticipant ConvPart = session.load(ConventionParticipant.class, id);
			transaction.commit();
			return ConvPart;
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}

	
	
	public static void deleteConventionParticipantByIDconv(Integer id) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
	        String qryString2 = "delete from ConventionParticipant c where c.convention.id=:idConv";
	        Query query2 = session.createQuery(qryString2);
	        query2.setParameter("idConv", id);
	        query2.executeUpdate();
	        
			transaction.commit();
	        session.clear();
	        session.close();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}
	
	public static void deleteConventionParticipant(Integer id) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			ConventionParticipant ConvPart = session.load(ConventionParticipant.class, id);
			if (ConvPart != null) {
				session.delete(ConvPart);
			}
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public static void updateConventionParticipant(ConventionParticipant ConvPart) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			session.update(ConvPart);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public static ConventionParticipant getConventionParticipantById(Integer id) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			ConventionParticipant ConvPart = session.byId(ConventionParticipant.class).getReference(id);
			transaction.commit();
			return ConvPart;
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}

}
