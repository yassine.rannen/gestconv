package API;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;

import models.Convention;
import models.Participant;
import utils.HibernateUtils;

public class APIConvention {

	
	@SuppressWarnings({ "unchecked", "deprecation" })  
	public static List<Convention> GetAllConventions() {
        try {
        	Session session = HibernateUtils.getSessionFactory().openSession();
        	Criteria crit =	 session.createCriteria(Convention.class);
        	List<Convention> Convs = crit.list();      
        	return Convs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
	}
	public static void SaveConvention(Convention C) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(C);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		}
	}

	public static Convention loadConvention(int id) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			Convention C = session.load(Convention.class, id);
			transaction.commit();
			return C;
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}

	public static void deleteConvention(int id) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			Convention C = session.load(Convention.class, id);
			if (C != null) {
				session.delete(C);
			}
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public static void updateConvention(Convention UpdatedConv) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			Query q = session.createQuery("from Convention where id= :idConv");
			q.setParameter("idConv", UpdatedConv.getId());

			Convention _dbConv = (Convention)q.getResultList().get(0);
			_dbConv.setDateVigueurConvention(UpdatedConv.getDateVigueurConvention());
			_dbConv.setDateExpirationConvention(UpdatedConv.getDateExpirationConvention());
			_dbConv.setDateEditionConvention(UpdatedConv.getDateEditionConvention());
			_dbConv.setType(UpdatedConv.getType());
			_dbConv.setObjet(UpdatedConv.getObjet());
			_dbConv.setListConventionParticipant(UpdatedConv.getListConventionParticipant());
			session.update(_dbConv);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public static Convention getConventionById(int id) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			Convention C = session.byId(Convention.class).getReference(id);
			transaction.commit();
			return C;
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}

}
