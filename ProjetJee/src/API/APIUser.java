package API;

import org.hibernate.Session;
import org.hibernate.Transaction;
import models.User;
import utils.HibernateUtils;

public class APIUser {

	public static void SaveUser(User U) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(U);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		}
	}

	public static User loadUser(int id) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			User U = session.load(User.class, id);
			transaction.commit();
			return U;
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}

	public static void deleteUser(int id) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			User U = session.load(User.class, id);
			if (U != null) {
				session.delete(U);
			}
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public static void updateUser(User U) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			session.update(U);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public static User getUserById(int id) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			User U = session.byId(User.class).getReference(id);
			transaction.commit();
			return U;
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}

}
