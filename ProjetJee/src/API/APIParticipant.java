package API;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import models.Participant;
import utils.HibernateUtils;

public class APIParticipant {

	@SuppressWarnings({ "unchecked", "deprecation" })  
	public static List<Participant> GetAllParticipant() {
        try {
        	Session session = HibernateUtils.getSessionFactory().openSession();
        	Criteria crit =	 session.createCriteria(Participant.class);
        	List<Participant> Partis = crit.list();      
        	return Partis;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
	}
	public static void SaveParticipant(Participant P) {
		Session sessionHib = HibernateUtils.getSessionFactory().openSession();
		Transaction tx = sessionHib.beginTransaction();
		try {
			sessionHib.save(P);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		}
	}

	public static Participant loadParticipant(int id) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			Participant P = session.load(Participant.class, id);
			transaction.commit();
			session.close();
			return P;
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}

	public static void deleteParticipant(int id) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			Participant P = session.load(Participant.class, id);
			if (P != null) {
				session.delete(P);
				System.out.println(P.getNom() + "is deleted");
			}
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public static void updateParticipant(Participant P) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			session.update(P);
			System.out.println(P.getNom() + "is updated");
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public static Participant getParticipantById(long id) {
		Transaction transaction = null;
		try {
			Session session = HibernateUtils.getSessionFactory().openSession();
			transaction = session.beginTransaction();

			Participant P = session.byId(Participant.class).getReference(id);
			transaction.commit();
			return P;
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
			return null;
		}
	}

}
