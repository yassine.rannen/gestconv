package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import API.APIConvention;
import models.Convention;
import models.ConventionParticipant;

/**
 * Servlet implementation class GetParticipantByConv
 */
@WebServlet("/GetParticipantByConv")
public class GetParticipantByConv extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetParticipantByConv() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		Convention conv = APIConvention.getConventionById(Integer.parseInt(id));
		Set<ConventionParticipant> convpartList = conv.getListConventionParticipant();
		String mylist="";
		for(ConventionParticipant cp:convpartList) {
			mylist = mylist + cp.getParticipant().getNom()+"_"+cp.getDateSignature()+"&";
		}
		System.out.println(mylist);
        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().write(mylist.replaceFirst(".$",""));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
