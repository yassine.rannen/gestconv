package controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import API.APIConvention;
import API.APIConventionParticipant;
import API.APIParticipant;
import models.Convention;
import models.ConventionParticipant;
import models.Participant;

/**
 * Servlet implementation class UpdateConv
 */
@WebServlet("/UpdateConv")
public class UpdateConv extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateConv() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int Id = Integer.parseInt(request.getParameter("idConv")) ;
		String type = request.getParameter("type");
		String objet= request.getParameter("objet");
		String[] Participants = request.getParameterValues("SelectedParticipant"); //fromat 'id_date'
		LocalDate DateEdition = null;
		LocalDate DateEntreEnVigeur = null;
		LocalDate DateExpiration = null;
		try {
			 DateEdition = LocalDate.parse(request.getParameter("DateEdition")); 
			 DateEntreEnVigeur = LocalDate.parse(request.getParameter("DateEntreEnVigeur")); 
			 DateExpiration = LocalDate.parse(request.getParameter("DateExpiration"));
		}catch(Exception ex) {
			request.setAttribute("erreur", "Server error : Date non vailde !");
			getServletContext().getRequestDispatcher("/Conventions.jsp").forward(request, response);
		}		
		try {
			if( Id!=0 &&type!=null && objet!=null && Participants !=null && DateEdition!=null && DateEntreEnVigeur !=null && DateExpiration !=null) {
				
				Convention Conv = new Convention();
				Conv.setId( Id);
				Conv.setType(type);
				Conv.setDateEditionConvention(DateEdition);
				Conv.setObjet(objet);
				Conv.setDateVigueurConvention(DateEntreEnVigeur);
				Conv.setDateExpirationConvention(DateExpiration);
				
				
				APIConventionParticipant.deleteConventionParticipantByIDconv(Id);//this line is to clear all conv_part manual delete cascading
				//we will use it until we found a solution for the cascading
				
				
				
				Set<ConventionParticipant> listParticipant = new HashSet<>();
				
				for(String participation : Participants) {
					
					String id_part = participation.split("_")[0];
					LocalDate DateSignature = LocalDate.parse(participation.split("_")[1]);
					
					if(id_part != null) {
						  try { 
							  int id = Integer.parseInt(id_part);//if this work then it's an existing
							  Participant P = APIParticipant.loadParticipant(id);
							  
							  ConventionParticipant ConvPart = new ConventionParticipant(P, Conv, DateSignature);
							  		  
							  listParticipant.add(ConvPart);					  
							  
						  }catch(Exception e) { 
							  e.printStackTrace();
							
							  String nomP = id_part;//get new part name 
							  Participant newPart = new Participant(nomP);
							  ConventionParticipant ConvPart = new  ConventionParticipant(newPart, Conv, DateSignature);
							  listParticipant.add(ConvPart);
							 
						  }	
					}
					
				}
				
				
				  Conv.setListConventionParticipant(listParticipant);
				  
				  APIConvention.updateConvention(Conv);
				 
				
				request.setAttribute("success", "La nouvelle convention est ajouter avec l'ID : "+Conv.getId());
				getServletContext().getRequestDispatcher("/Conventions.jsp").forward(request, response);
			}else {
				request.setAttribute("erreur", "Server error : Tous les champs sont obligatoires !");

				getServletContext().getRequestDispatcher("/Conventions.jsp").forward(request, response);
			}
		}catch(Exception e) {
			e.printStackTrace();
			request.setAttribute("erreur", "Server error : Impossible de modifier cette convention a cause d'une erreur serveur !");

			getServletContext().getRequestDispatcher("/Conventions.jsp").forward(request, response);
		}
	}

}
