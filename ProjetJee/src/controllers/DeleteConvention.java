package controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import API.APIConvention;
import API.APIConventionParticipant;

/**
 * Servlet implementation class DeleteConvention
 */
@WebServlet("/DeleteConvention")
public class DeleteConvention extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteConvention() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect(request.getContextPath() + "/Conventions.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String id = request.getParameter("idConventionDelete");
			if(id!=null) {
				APIConventionParticipant.deleteConventionParticipantByIDconv(Integer.parseInt(id));
				APIConvention.deleteConvention(Integer.parseInt(id));
				request.setAttribute("success", "La Convention est supprim� ");
				getServletContext().getRequestDispatcher("/Conventions.jsp").forward(request, response);
			}else {
				request.setAttribute("erreur", "Server error : Impossible d'ajouter cette convention a cause d'une erreur serveur !");

				getServletContext().getRequestDispatcher("/Conventions.jsp").forward(request, response);
			}
		}catch(Exception e) {
			request.setAttribute("erreur", "Server error : Impossible d'ajouter cette convention a cause d'une erreur serveur !");

			getServletContext().getRequestDispatcher("/Conventions.jsp").forward(request, response);
		}

	}

}
