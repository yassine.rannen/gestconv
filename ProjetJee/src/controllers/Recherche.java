package controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import API.APIMultiSearchCriteria;
import models.Convention;
import models.RechercheModel;

/**
 * Servlet implementation class Recherche
 */
@WebServlet("/Recherche")
public class Recherche extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Recherche() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		getServletContext().getRequestDispatcher("/Recherche.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String KeyWord = request.getParameter("KeyWord");
		HttpSession httpSession = request.getSession();
		if (KeyWord != null) {
			System.out.println("key word "+KeyWord);
			//recherche par mot cl�
			HashSet<Convention> result = APIMultiSearchCriteria.getConventionsByKeyWord(KeyWord);
			if(result!=null) {
				httpSession.setAttribute("ListRes", result);
				httpSession.setAttribute("success",result.size()+ " Conventions troun�s");
			}else {
				httpSession.setAttribute("ListRes", null);
				httpSession.setAttribute("success","Accune resultat trouv�es");				
			}
			getServletContext().getRequestDispatcher("/Recherche.jsp").forward(request, response);
		}else {
			//recherche avanc�e
			System.out.println("key word avanc�e");
			String type = "%";
			String objet="%";
			String ID_part = "%"; //fromat 'id_date'
			String DateEdition = "%";
			String DateEntreEnVigeur = "%";
			String DateExpiration = "%";
			String dateSignature = "%";
			try {
				 type = request.getParameter("type");
				 objet= "%"+request.getParameter("objet")+"%";
				 ID_part = request.getParameter("ParticipantsListe");
				 DateEdition = request.getParameter("DateEdition"); 
				 DateEntreEnVigeur = request.getParameter("DateEntreEnVigeur"); 
				 DateExpiration = request.getParameter("DateExpiration");
				 dateSignature = request.getParameter("DateSignature");
				 
				 if(objet=="")objet="%";
				 if(DateEdition=="")DateEdition="%";
				 if(DateEntreEnVigeur=="")DateEntreEnVigeur="%";
				 if(DateExpiration=="")DateExpiration="%";
				 if(dateSignature=="")dateSignature="%";
				 
			}catch(Exception ex) {
				request.setAttribute("erreur", "Server error : une des dates est non vailde !");
				getServletContext().getRequestDispatcher("/Recherche.jsp").forward(request, response);
			}
			
			HashSet<Convention> result = APIMultiSearchCriteria.getConventionsByCustomSearch(type,DateEdition,objet,DateEntreEnVigeur,DateExpiration,dateSignature,ID_part);
			if(result!=null) {
				httpSession.setAttribute("ListRes", result);
				httpSession.setAttribute("success",result.size()+ " Conventions troun�s");
			}else {
				httpSession.setAttribute("ListRes", null);
				httpSession.setAttribute("success","Accune resultat trouv�es");				
			}
			getServletContext().getRequestDispatcher("/Recherche.jsp").forward(request, response);
			System.out.println(result.size());
		}
		
	}

}
