package controllers;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import models.User;
import utils.Hasher;
import utils.HibernateUtils;

/**
 * Servlet implementation class AddUser
 */
@WebServlet("/AddUser")
public class AddUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		  HibernateUtils.shutdown();
		 //this code is used only once, to create the two users ahmed and yassine 
		  String usr = request.getParameter("UserName"); String mdp =
		  request.getParameter("password");
		  
		  if (!usr.equals("") && !mdp.equals("")) { String HashedPassword=""; try {
		  HashedPassword = Hasher.Hash(mdp); } catch (NoSuchAlgorithmException e) {
		  request.setAttribute("erreur","Erreur temporaire dans le serveur");
		  getServletContext().getRequestDispatcher("" + "/Login.jsp").forward(request,
		  response); } 
		  User user = new User(usr,HashedPassword); 
		  SessionFactory sf = HibernateUtils.getSessionFactory(); 
		  org.hibernate.Session sessionHib = sf.openSession(); 
		  Transaction tx = sessionHib.beginTransaction(); 
		  try {
		  sessionHib.persist(user); 
		  tx.commit(); 
		  } catch (Exception e) {
			  tx.rollback();
		  
			  e.printStackTrace(); 
			  }
		  
		  }
		 
	}

}
