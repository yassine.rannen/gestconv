package controllers;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import API.APIConvention;
import models.Convention;
import models.ConventionParticipant;
import models.Participant;
import models.TypeConvention;
import models.User;
import utils.Hasher;
import utils.HibernateUtils;

/**
 * Servlet implementation class loginUser
 */
@WebServlet("/loginUser")
public class loginUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public loginUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login = request.getParameter("UserName");
		String mdp = request.getParameter("password");
		if (login.equals("") && mdp.equals(""))
			response.sendRedirect("Conventions.jsp");
		else {
			String HashedPassword="";
			try {
				HashedPassword = Hasher.Hash(mdp);
			} catch (NoSuchAlgorithmException e) {
				request.setAttribute("erreur","Erreur temporaire dans le serveur");
				getServletContext().getRequestDispatcher(""
						+ "/login.jsp").forward(request, response);
			}
			SessionFactory sf = HibernateUtils.getSessionFactory();
			org.hibernate.Session sessionHib = sf.openSession();
			Query query = sessionHib.createNamedQuery("LoginUser");
			query.setParameter("usr", login);
			query.setParameter("pwd",HashedPassword);
			User connectedUser = null;
			try {
				connectedUser = (User)query.getResultList().get(0);
			}catch(Exception e) {
				
			}		 
			if (connectedUser!=null) {
				HttpSession httpSession = request.getSession();
				httpSession.setAttribute("CurrentUser", connectedUser);
				response.sendRedirect("Conventions.jsp");
			} else {
				request.setAttribute("erreur","Invalid Login/MDP !");
				getServletContext().getRequestDispatcher(""
						+ "/login.jsp").forward(request, response);
			}
			
		}
	}

}
