package controllers;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import API.APIParticipant;
import models.Participant;
import models.User;
import utils.Hasher;
import utils.HibernateUtils;

/**
 * Servlet implementation class addParticipant
 */
@WebServlet("/addParticipant")
public class addParticipant extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public addParticipant() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		  String nom = request.getParameter("nom"); 
		  if (nom.equals(""))
		  response.sendRedirect("Conventions.jsp");
		  else {
			  Participant P = new Participant(nom); 
			  APIParticipant.SaveParticipant(P);
			  response.sendRedirect("Conventions.jsp"); 
			 }
		 
	}
	
	

}
