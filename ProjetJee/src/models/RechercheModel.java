package models;

import java.util.List;

public class RechercheModel {
	private List<Convention> result;

	public List<Convention> getResult() {
		return result;
	}

	public void setResult(List<Convention> result) {
		result = result;
	}

	public RechercheModel(List<Convention> result) {
		result = result;
	}
	
}
