package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@Table(name = "APP_USER")
@NamedQueries(value = {
		@NamedQuery(name = "LoginUser", query = "From User u where u.userName= :usr and u.password= :pwd") })
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String userName = "";
	private String password = "";;

	public User() {
	}

	public User(String UserName, String Password) {
		this.userName = UserName;
		this.password = Password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
